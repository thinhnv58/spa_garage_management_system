import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsultantViewComponent } from './consultant-view.component';
import { ManagerViewComponent } from './manager-view.component';
import { JobDetailComponent } from './job-detail.component'; 
const appRoutes: Routes = [
  {
    path: 'manager',
    component: ManagerViewComponent
  },
  {
    path: 'consultant',
    component: ConsultantViewComponent
  },

  {
    path: 'job-detail/:id',
    component: JobDetailComponent
  },
  
  {
    path: '',
    redirectTo: '/consultant',
    pathMatch: 'full'
  },


];



export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
