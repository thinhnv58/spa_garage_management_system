"use strict";
var router_1 = require('@angular/router');
var consultant_view_component_1 = require('./consultant-view.component');
var manager_view_component_1 = require('./manager-view.component');
var job_detail_component_1 = require('./job-detail.component');
var appRoutes = [
    {
        path: 'manager',
        component: manager_view_component_1.ManagerViewComponent
    },
    {
        path: 'consultant',
        component: consultant_view_component_1.ConsultantViewComponent
    },
    {
        path: 'job-detail/:id',
        component: job_detail_component_1.JobDetailComponent
    },
    {
        path: '',
        redirectTo: '/consultant',
        pathMatch: 'full'
    },
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map