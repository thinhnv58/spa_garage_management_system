"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var common_1 = require('@angular/common');
var job_service_1 = require('./job.service');
var JobDetailComponent = (function () {
    function JobDetailComponent(jobService, route, location) {
        this.jobService = jobService;
        this.route = route;
        this.location = location;
    }
    JobDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.forEach(function (params) {
            var id = +params['id'];
            _this.jobService.getJob(id)
                .then(function (job) { return _this.job = job; });
        });
    };
    JobDetailComponent.prototype.save = function () {
        this.goBack();
    };
    JobDetailComponent.prototype.goBack = function () {
        this.location.back();
    };
    JobDetailComponent = __decorate([
        core_1.Component({
            selector: 'job-detail',
            template: "\n    <h3>Job detail</h3>\n    <div *ngIf=\"job\">\n      <div>\n        <label>ID: </label>{{job.id}}\n      </div>\n      <div>\n        <label>Repair List: </label><input [(ngModel)]=\"job.repair_list\"  placeholder=\"repair list \" />\n      </div>\n      \n      <button (click)=\"goBack()\">Back</button>\n      <button (click)=\"save()\">Save</button>\n      \n    </div>\n  "
        }), 
        __metadata('design:paramtypes', [job_service_1.JobService, router_1.ActivatedRoute, common_1.Location])
    ], JobDetailComponent);
    return JobDetailComponent;
}());
exports.JobDetailComponent = JobDetailComponent;
//# sourceMappingURL=job-detail.component.js.map