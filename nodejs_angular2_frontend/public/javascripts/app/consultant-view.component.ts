import { Component } from '@angular/core';
@Component({
  selector: 'consultant-view',
  template: `
    <jobs>load list jobs ...</jobs>

    <div class="fixed-action-btn horizontal click-to-toggle" style="bottom: 45px; right: 24px;">
      <a class="btn-floating btn-large red">
        <i class="material-icons">add</i>
      </a>
      <ul>
        <li><a class="btn-floating blue" ><i class="material-icons">supervisor_account</i></a></li>
        <li><a class="btn-floating green"><i class="material-icons">work</i></a></li>
      </ul>
    </div>
        
  `
})
export class ConsultantViewComponent { }
