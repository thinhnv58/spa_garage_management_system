import { Component } from '@angular/core';
@Component({
  selector: 'my-app',
  template: `

    <nav class="teal" role="navigation">
        <div class="nav-wrapper container">
        <a id="logo-container" href="" class="brand-logo">SPA</a>
        <ul class="right hide-on-med-and-down">
            <li><a routerLink="/consultant">Consultant view</a></li>
            <li><a routerLink="/manager">Manager view</a></li>
        </ul>

        <ul id="nav-mobile" class="side-nav">
            <li><a routerLink="/consultant">Consultant view</a></li>
            <li><a routerLink="/manager">Manager view</a></li>
            <li><a routerLink="/job-detail/12">job detail</a></li>
            
            
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
        </div>
    </nav>
    <div class="container">
        <router-outlet></router-outlet>
    </div>
    

    
  `
})
export class AppComponent { }
