import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { Job } from './job';
import { JobService } from './job.service';

@Component({
  selector: 'job-detail',
  template: `
    <h3>Job detail</h3>
    <div *ngIf="job">
      <div>
        <label>ID: </label>{{job.id}}
      </div>
      <div>
        <label>Repair List: </label><input [(ngModel)]="job.repair_list"  placeholder="repair list " />
      </div>
      
      <button (click)="goBack()">Back</button>
      <button (click)="save()">Save</button>
      
    </div>
  `
})
export class JobDetailComponent implements OnInit{
  job: Job;
  constructor (
    private jobService: JobService,
    private route: ActivatedRoute,
    private location: Location
    ){}


  ngOnInit(): void{
    this.route.params.forEach((params: Params) => {
      let id = +params['id'];
      this.jobService.getJob(id)
        .then(job => this.job = job);
    }); 
  }

  save(): void{
    this.goBack();
  }
  goBack(): void{
    this.location.back();
  }
 }
