import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Job } from './job';
import { JobService } from './job.service';
@Component({
  selector: 'jobs',
  template: `
    <ul class="collection">
      <li class="collection-item avatar"
        *ngFor="let job of jobs"
      >
        <img src="images/customer.jpg" alt="" class="circle">
        <span class="title">{{ job.repair_list }}</span>
        
        <a href="/job-detail/{{job.id}}" class="secondary-content" ><i class="material-icons">mode_edit</i></a>
      </li>
    </ul>
  `
})
export class JobsComponent implements OnInit{
  constructor (
    private jobService: JobService,
    private router: Router){}
  private jobs: Job[];

  selectJob(job: Job): void{

    let link = ['/job-detail', job.id];
    console.log(link);
    this.router.navigate(link);

  }
  ngOnInit(): void{
    this.jobService.getJobs().then(jobs => this.jobs = jobs);
  }
 }
