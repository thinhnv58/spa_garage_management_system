import { Job } from './job';

export const JOBS: Job[] = [
  {id: 11, repair_list: 'Súc rửa két nước làm mát ( Bao gồm hóa chất xúc rửa và hóa chất pha chống lắng cặn )'},
  {id: 12, repair_list: 'Công thay dầu bôi trơn động cơ'},
  {id: 13, repair_list: 'Đại tu máy: làm hơi, xi lanh ...'},
  {id: 14, repair_list: 'Đại tu hệ thống điện ( Không bao gồm tiền dây điện, rắc cắm, cầu chì ...)'},
  {id: 15, repair_list: 'Hạ hộp số ( sửa, thay lá côn, bàn ép, bi T ... ) Hạng mục này sẽ có giá cao hơn chút với cấu tạo phức tạp hơn thong thường, hộp số tự động ...'},
  {id: 16, repair_list: 'Thay căn chỉnh dây cua roa, bi tăng, bi tì ...trường hợp đã bị đứt dây cua roa.'},
  {id: 17, repair_list: 'Bảo dưỡng vệ sinh kim phun, bướm ga, cổ hút'},
  {id: 18, repair_list: 'Công thay dầu bôi trơn động cơ'},
  {id: 19, repair_list: 'Công kiểm tra toàn bộ kỹ thuật của xe, lên phương án sửa chữa'},
  {id: 20, repair_list: 'Bảo dưỡng phành 04 bánh (  Không bao gồm tiền công láng đĩa phành ) xe 5 chỗ.'}
];
