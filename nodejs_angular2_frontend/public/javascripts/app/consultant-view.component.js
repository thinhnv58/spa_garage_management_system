"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ConsultantViewComponent = (function () {
    function ConsultantViewComponent() {
    }
    ConsultantViewComponent = __decorate([
        core_1.Component({
            selector: 'consultant-view',
            template: "\n    <jobs>load list jobs ...</jobs>\n\n    <div class=\"fixed-action-btn horizontal click-to-toggle\" style=\"bottom: 45px; right: 24px;\">\n      <a class=\"btn-floating btn-large red\">\n        <i class=\"material-icons\">add</i>\n      </a>\n      <ul>\n        <li><a class=\"btn-floating blue\"><i class=\"material-icons\">supervisor_account</i></a></li>\n        <li><a class=\"btn-floating green\"><i class=\"material-icons\">work</i></a></li>\n      </ul>\n    </div>\n        \n  "
        }), 
        __metadata('design:paramtypes', [])
    ], ConsultantViewComponent);
    return ConsultantViewComponent;
}());
exports.ConsultantViewComponent = ConsultantViewComponent;
//# sourceMappingURL=consultant-view.component.js.map