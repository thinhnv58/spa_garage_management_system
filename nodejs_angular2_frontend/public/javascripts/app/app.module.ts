import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { routing  } from './app.routing';

import { AppComponent } from './app.component';
import { JobsComponent } from './jobs.component';
import { JobDetailComponent } from './job-detail.component';
import { ConsultantViewComponent } from './consultant-view.component';
import { ManagerViewComponent } from './manager-view.component';

import { JobService } from './job.service';
@NgModule({
  imports:      [ 
    BrowserModule,
    HttpModule,
    FormsModule,
    routing,

     ],
  declarations: [
    AppComponent,
    JobsComponent,
    JobDetailComponent,
    ConsultantViewComponent,
    ManagerViewComponent,
    
    ],
  bootstrap: [AppComponent],
  providers: [JobService],

})
export class AppModule { }
