"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var job_service_1 = require('./job.service');
var JobsComponent = (function () {
    function JobsComponent(jobService, router) {
        this.jobService = jobService;
        this.router = router;
    }
    JobsComponent.prototype.selectJob = function (job) {
        var link = ['/job-detail', job.id];
        console.log(link);
        this.router.navigate(link);
    };
    JobsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.jobService.getJobs().then(function (jobs) { return _this.jobs = jobs; });
    };
    JobsComponent = __decorate([
        core_1.Component({
            selector: 'jobs',
            template: "\n    <ul class=\"collection\">\n      <li class=\"collection-item avatar\"\n        *ngFor=\"let job of jobs\"\n      >\n        <img src=\"images/customer.jpg\" alt=\"\" class=\"circle\">\n        <span class=\"title\">{{ job.repair_list }}</span>\n  \n        <a href=\"/job-detail/{{job.id}}\" class=\"secondary-content\" ><i class=\"material-icons\">mode_edit</i></a>\n      </li>\n    </ul>\n\n  "
        }), 
        __metadata('design:paramtypes', [job_service_1.JobService, router_1.Router])
    ], JobsComponent);
    return JobsComponent;
}());
exports.JobsComponent = JobsComponent;
//# sourceMappingURL=jobs.component.js.map